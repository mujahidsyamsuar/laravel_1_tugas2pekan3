<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('form');
    }
    
    public function welcome(Request $request){
        $name1 = $request->nama1;
        $name2 = $request->nama2;
        return view('home', compact('name1', 'name2'));
    }
}
