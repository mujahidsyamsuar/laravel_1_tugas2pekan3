@extends('layout.master')

@section('title')
    Buat Account Baru!
@endsection
   
@section('content')
    <h2><b>Sign Up Form</b></h2>
    <form action="/welcome" method="POST">
        @csrf
        <label for="title">First Name:</label>
        <br><br>
        <input type="text" name="nama1" id="title"> <br><br>
        <label for="title">Last Name:</label>
        <br><br>
        <input type="text" name="nama2" id="title">
        <br><br>
        <label for="gender">Gender:</label><br><br>
        <input type="radio" value="Male" name="gender">Male<br>
        <input type="radio" value="Female" name="gender">Female<br>
        <input type="radio" value="Other" name="gender">Other
        <br><br>
        <label for="nat">Nationality:</label><br>
        <select name="nat" id="nat">
            <option value="indonesia">Indonesian</option>
            <option value="other">Other</option>
        </select>
        <br><br>
        <label for="lang">Language Spoken:</label>
        <br><br>
        <input type="checkbox" id="ls1" name="ls1">
        <label for="ls1"> Bahasa Indonesia</label><br>
        <input type="checkbox" id="ls2" name="ls2">
        <label for="ls1"> English</label><br>
        <input type="checkbox" id="ls3" name="ls3">
        <label for="ls1"> Other</label>
        <br><br>
        <label for="bio">Bio:</label>
        <br><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">
    
    </form>

@endsection
